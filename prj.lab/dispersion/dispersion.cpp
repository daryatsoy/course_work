﻿#define _USE_MATH_DEFINES
#include <opencv2/opencv.hpp>
#include <iostream>
#include <opencv2/ximgproc.hpp>
#include <math.h>
#include <ctime>
using namespace cv;
using namespace std;
using namespace cv::ximgproc;

//Экземпляром класса Piece является единичная частица superpixels, которая в последствие превращается в однородный шарик
class Piece {
public:
    Mat pattern; // изображение частицы
    Mat mask; // маска частицы
    Mat current_pattern; // текущее преобразованное изображение частицы
    Mat current_mask; // текущая преобразованная маска частицы
    Rect location; // первоначальное местоположение частицы
    int current_z; // Текущая координата по оси, перпендикулярной экрану

    Piece(Mat& _src, Mat& _mask, Rect _location, Rect _canvas_size, int _duration, int _delay=0, float _inflate=1.) {
        pattern = _src.clone();
        mask = _mask.clone();
        current_dil = _mask.clone();
        current_er = _mask.clone();
        duration = _duration;
        location = _location;
        delay = _delay;
        inflate = _inflate;
        pattern_center = Point2i(location.x + location.width / 2, location.y + location.height / 2);
        color = mean(_src, _mask);
        color_mask = Mat(pattern.size(), CV_8UC3);
        color_mask.setTo(color);
        canvas_size = _canvas_size;
        canvas_center = Point2i(canvas_size.br().x / 2, canvas_size.br().y / 2);
        count_destination();
        count_steps();
        count_circle();
    };

    // задать максимальное значение смещения частицы за границу исходного изображения
    void setOffsetR(int _r) {
        offset_r = _r;
    };

    // задать задержку в кадрах перед началом смещения
    void setDelay(int _delay) {
        delay = _delay;
    };

    // задать точку проекции
    void setZc(int _zc) {
        zc = _zc;
    };

    // Проекция на плоскость экрана в текущий момент времени  _n_frame
    Rect2i projection(int _n_frame) {
        current_mask.release();
        current_pattern.release();
        double weight_color = min(1., 2. / duration_piece * _n_frame);
        addWeighted(pattern, 1 - weight_color, color_mask, weight_color, 0, current_pattern);
        float r = -1. / zc;
        int timing = max(0, _n_frame - delay);
        int x3d = pattern_center.x + timing * step_x - canvas_center.x;
        int y3d = pattern_center.y + timing * step_y - canvas_center.y;
        current_z = timing * step_z;
        float dividend = r * current_z + 1;
        if (abs(dividend) > 0.01) {
            int tl_x = int(float(x3d) / dividend + canvas_center.x);
            int tl_y = int(float(y3d) / dividend + canvas_center.y);
            int new_width = int(float(pattern.size().width) / dividend);
            int new_height = int(float(pattern.size().height) / dividend);
            double grow = 1.;
            if (timing > 3) {
                grow = pow(inflate, timing - 2);
                new_width = int(float(pattern.size().width * grow) / dividend);
                new_height = int(float(pattern.size().height * grow) / dividend);
            }
            int tmp_x = tl_x - new_width / 2;
            int tmp_y = tl_y - new_height / 2;
            int cut_x = max(0, tmp_x);
            int cut_y = max(0, tmp_y);
            int cut_width = max(0, min(min(new_width, canvas_size.width - tmp_x), tmp_x + new_width));
            int cut_height = max(0,min(min(new_height, canvas_size.height - tmp_y), tmp_y + new_height));
            if (cut_height > 0 && cut_width > 0) {
                resize(current_pattern, current_pattern, Size(new_width, new_height));
                resize(mask, current_mask, Size(new_width, new_height));
                if (timing > 3) {
                    dilate(current_dil, current_dil, element);
                    erode(current_er, current_er, element);
                    resize(current_dil, current_dil_b, Size(new_width, new_height));
                    resize(current_er, current_er_b, Size(new_width, new_height));
                    circle_mask.release();
                    circle_mask = Mat::zeros(Size(new_width, new_height), CV_8UC1);
                    int rnew = r_circle * grow / dividend;
                    circle(circle_mask, Point(int(c_x * new_width / mask.size().width), int(c_y * new_height / mask.size().height)), rnew, Scalar::all(255), -1); 
                    bitwise_and(circle_mask, current_dil_b, current_mask);
                    bitwise_or(current_mask, current_er_b, current_mask);
                }
                Rect2i new_place;
                if (new_width > canvas_size.width || new_height > canvas_size.height) {
                    new_place = Rect2i(max(0, cut_x), max(0, cut_y),
                        min(cut_width, canvas_size.width - max(0, cut_x)), min(cut_height, canvas_size.height - max(0, cut_y)));
                    current_pattern = current_pattern(Rect2i(max(0, cut_x - abs(tmp_x)), max(0, cut_y - abs(tmp_y)),
                        min(cut_width, canvas_size.width - max(0, cut_x - abs(tmp_x))),
                        min(cut_height, canvas_size.height - max(0, cut_y - abs(tmp_y)))));
                    current_mask = current_mask(Rect2i(max(0, cut_x - abs(tmp_x)), max(0, cut_y - abs(tmp_y)),
                        min(cut_width, canvas_size.width - max(0, cut_x - abs(tmp_x))),
                        min(cut_height, canvas_size.height - max(0, cut_y - abs(tmp_y)))));
                }
                else {
                    new_place = Rect2i(cut_x, cut_y, cut_width, cut_height);
                    current_pattern = current_pattern(Rect2i(cut_x - tmp_x, cut_y - tmp_y, cut_width, cut_height));
                    current_mask = current_mask(Rect2i(cut_x - tmp_x, cut_y - tmp_y, cut_width, cut_height));
                }
                return new_place;
            }
            else {
                return Rect2i(0, 0, 0, 0);
            }
        }
        else {
            return Rect2i(0, 0, 0, 0);
        }
    };

private:
    Scalar color; // итоговый цвет частицы
    Rect canvas_size; // размер исходного изображения
    Point2i canvas_center; // центр исходного изображения
    Point2i pattern_center; // центр частицы на холсте
    int offset_r = 10; // максимальное смещение частицы за границу исходного изображения
    int duration; // за сколько кадров частицы придет к конечной точке
    int duration_piece; // сколько кадров частица в полете (без начальной задержки)
    int delay; // через сколько кадров от запуска частица начнет смещение
    Point3i destination; // конечная точка пути частицы в пространстве
    Mat color_mask; // цветная маска преобрзаованной частицы
    Mat circle_mask; // круглая маска преобрзаованной частицы
    int r_circle; // первоначальный радиус кружка частицы
    int zc=-500; // координата камеры по оси, перпендикулярной экрану
    int step_x, step_y, step_z; // велечина изменения координат за один шаг (кадр)
    Mat current_dil, current_er; // текущая расширенная и суженная маски частицы
    Mat current_dil_b, current_er_b; // текущая расширенная и суженная маски частицы с проективным увеличением
    int c_x, c_y; // центр масс частицы
    Mat element = getStructuringElement(MORPH_ELLIPSE, Size(5, 5)); // Ядро морфологических фильтров
    float inflate; // Значене, во сколько раз увеливать частицу за кадр (по умолчанию 1)

    int random(int min, int max) {
        return round(float(rand() * (max - min)) / RAND_MAX + min);
    }

    // расчитать точку конечного местоположения частицы
    void count_destination() {
        int alpha = random(0, 360); // угол смещения
        int tetha = random(0, 360); // угол смещения
        int max_side = int(max(float(canvas_size.br().x) / 2, float(canvas_size.br().y / 2)));
        int r = random(max_side, max_side + offset_r); // радиус смещения
        destination = Point3i(                 // конечная точка перемещения частицы
            int(r * sin(tetha * M_PI / 180) * cos(alpha * M_PI / 180) + canvas_center.x),
            int(r * sin(tetha * M_PI / 180) * sin(alpha * M_PI / 180) + canvas_center.y),
            int(r * cos(tetha * M_PI / 180))
            );
    };
    
    // расчитать смещение за один кадр
    void count_steps() {
        duration_piece = max(0, duration - delay);
        step_y = int(float(pattern_center.y - destination.y) / duration_piece);
        step_x = int(float(pattern_center.x - destination.x) / duration_piece);
        step_z = int(float(destination.z) / duration_piece);
    };

    // Расчет радуса кружка
    void count_circle() {
        // Площадь частицы в пикселях
        int area = countNonZero(mask);
        // Радиус кружка с площадью, равной площади текущей частицы
        r_circle = int(sqrt(area / M_PI));
        Moments m = moments(mask, true); // центр масс частицы
        // координаты центра итогового кружка
        c_x = m.m10 / m.m00;
        c_y = m.m01 / m.m00;
    }

};

// Генертор эфекта
class Dispersion {
public:
    Dispersion(Mat _src, Mat1b _mask, Mat _background, int _duration=48, int _fps = 16, float _inflate = 1.) {
        srand((int)time(0));
        img = _src;
        img_mask = _mask;
        img_back = _background;
        n_frame = _duration;
        fps = _fps;
        inflate = _inflate;
        Generate_superpixels();
    }

    // Изменение параметров генерации суперпикселей
    bool ResetParametrsSuperpixels(int _region_size = 20, int _ruler = 30,
                                   int _min_element_size = 50, int _num_iterations = 3) {
        if (_region_size > 1) {
            region_size = _region_size;
        }
        else {
            return false;
        }
        if (_ruler > 1) {
            ruler = _ruler;
        }
        else {
            return false;
        }
        if (_min_element_size > 1) {
            min_element_size = _min_element_size;
        }
        else {
            return false;
        }
        if (_num_iterations > 0) {
            num_iterations = _num_iterations;
        }
        else {
            return false;
        }
        Generate_superpixels();
        return true;
    }

    // Запись видео
    void WriteVideo(string name="dispersion") {
        Rect2i replace; // Рамка для вставки частицы
        Mat frame, mask_frame; // Кадр и маска кадра
        VideoWriter video = VideoWriter(name + ".mp4", VideoWriter::fourcc('m', 'p', '4', 'v'), fps, img.size());
        video.write(img);
        for (int f = 0; f < n_frame; f++) {
            frame.release();
            frame = img.clone();
            sort(elements.begin(), elements.end(), compare_z()); // Сортируем частицы по оси, перпендикулярной экрану
            for (int i = 0; i < elements.size(); i++) {
                // Где частицы уже "отвалились", дорисовываем фон
                img_back(elements[i].location).copyTo(frame(elements[i].location), elements[i].mask);
            }
            for (int i = 0; i < elements.size(); i++) { 
                // Рамка для нового места частицы
                replace = elements[i].projection(f);
                if (replace.width > 0 && replace.height > 0) {
                    elements[i].current_pattern.copyTo(frame(replace), elements[i].current_mask);
                }
            }
            video.write(frame);
            cout << "frame\t" << f << endl;
            //imshow("t", frame);
            //waitKey(0);
        }
        video.release();
    }

private:
    Mat img; // Исходное изображение
    Mat1b img_mask; // Маска изображения
    Mat img_back; // Изображение с закращенным фоном
    int n_frame; // Количество кадров
    int fps; // Количество кадров в секнду
    int region_size = 20; // Среднее количество пикселей суперпикселя
    int ruler = 30; // Фактор суперпиксельной гладкости
    int min_element_size = 50; // Минимальный количество изолированных пикселей в процентах, 
                               // который должен быть поглощен в больший суперпиксель.
                               // min_element_size/100 * region_size
    int num_iterations = 3; // Количество итераций для уточнения границ уперпикселя
    Ptr<SuperpixelSLIC> slic; // Указатель на экземпляр суперпиксельной мозаики
    vector<Piece> elements; // Вектор частиц
    float inflate; // Значене, во сколько раз увеливать частицу за кадр (по умолчанию 1)

    // Генерация мозаики
    void Generate_superpixels() {
        if (slic) delete slic; // Если установлены новые параметры после создания экземпляра Dispersion
        if (elements.size() != 0) elements.clear();
        Mat converted;
        cvtColor(img, converted, COLOR_BGR2HSV);
        //Mat test = Mat::zeros(img.size(), img.type());
        slic = createSuperpixelSLIC(converted, SLICO, region_size, float(ruler));
        slic->iterate(num_iterations);
        slic->enforceLabelConnectivity(min_element_size);
        //Mat testmask;
        //slic->getLabelContourMask(testmask);
        //test = img.clone();
        //test.setTo(Scalar(255, 255, 255), testmask);
        //imwrite("mask_SLICO.png", test);
        Mat labels;
        slic->getLabels(labels);
        Mat1b mask_for_ith_sp;
        Rect boundRect;
        int n_pieces = slic->getNumberOfSuperpixels();
        int n_set = int(n_pieces / 20); // Частицы отлетают в 20 партий размером n_set
        for (int i = 0; i < n_pieces; ++i) {
            mask_for_ith_sp = (labels == i); // Маска одной частицы
            bitwise_and(mask_for_ith_sp, img_mask, mask_for_ith_sp); // Учитываем частицы, попавшие на объект
            //test.setTo(mean(img, mask_for_ith_sp), mask_for_ith_sp);
            if (countNonZero(mask_for_ith_sp) > 16) { // Если частица не на фоне
                boundRect = boundingRect(mask_for_ith_sp); // Создаем экземпляр частицы в минимальной рамке
                boundRect = Rect2i(max(0, boundRect.x - boundRect.width / 2), max(0, boundRect.y - boundRect.height),
                    min(2 * boundRect.width, img.size().width - max(0, boundRect.x - boundRect.width / 2)),
                    min(2 * boundRect.height, img.size().height - max(0, boundRect.y - boundRect.height / 2)));
                elements.push_back(Piece(img(boundRect), mask_for_ith_sp(boundRect),
                    boundRect, Rect(0, 0, img.size().width, img.size().height), n_frame, int(i / n_set), inflate));
            }
        }
        //imwrite("testmean.png", test);
        cout << "Labels generated successful!" << endl;
    };

    struct compare_z
    {
        inline bool operator() (const Piece& struct1, const Piece& struct2)
        {
            return (struct1.current_z > struct2.current_z);
        }
    };
};

int main() {
    Mat orig(imread("../../../testdata/orig.png")); 
    Mat1b mask(imread("../../../testdata/mask.jpg"));
    Mat back(imread("../../../testdata/back.png"));
    mask.convertTo(mask, CV_8UC1);
    int h = 800;
    resize(orig, orig, Size(orig.cols * h / orig.rows, h));
    resize(mask, mask, Size(orig.cols * h / orig.rows, h));
    resize(back, back, Size(orig.cols * h / orig.rows, h));
    Dispersion test = Dispersion(orig, mask, back, 48, 16, 1.03);
    test.WriteVideo("grow");
  waitKey(0);
  return 0;
}
